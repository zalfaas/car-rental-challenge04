class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="card mb-4">
        <div class="card-body">
          <img class="card-img-top mb-4" src="${this.image}" alt="${this.manufacture}" style="height: 280px; object-fit: cover;">
          <p style="font-weight: 400;">${this.model}</p>
          <p style="font-weight: 700;">Rp ${this.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} / hari</p>
          <p style="font-weight: 300;">
            ${this.description.slice(0, 38)}            </p>
          <p><img src="images/fi_users.png" class="me-2" width="16px">${this.capacity} orang</p>
          <p><img src="images/fi_settings.png" class="me-2" width="16px">${this.transmission}</p>
          <p><img src="images/fi_calendar.png" class="me-2" width="16px">Tahun ${this.year}</p>
          <button id="pilihMobil" type="button" class="btn btn-success w-100">Pilih Mobil</button>
        </div>
      </div>
    `;
  }
}
