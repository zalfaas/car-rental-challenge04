class App {
  constructor() {
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.tipeDriver = document.getElementById("tipe-driver");
    this.tanggalSewa = document.getElementById("tanggal-sewa");
    this.jamSewa = document.getElementById("jam-sewa");
    this.jumlahPenumpang = document.getElementById("jumlah-penumpang");
  }

  async init() {
    await this.load();

    // Register click listener
    this.loadButton.onclick = this.run;
  }

  run = async () => {
    await this.load();
    // To clear data every search
    this.clear();

    const node = document.createElement("div");
    node.className = "row";
    this.carContainerElement.className = "container";
    const baru = document.createElement("div");
    baru.className = "container";
    node.appendChild(baru);
    
    const baris = document.createElement("div");
    baris.className = "row";
    this.carContainerElement.appendChild(baris);
    Car.list.forEach((car) => {
      const col = document.createElement("div");
      col.className = "col-lg-4 col-md-12 col-sm-12"
      col.innerHTML = car.render();
      baris.appendChild(col);
    });
  };

  async load() {
    const tanggal = this.tanggalSewa.value;
    const jam = this.jamSewa.value;
    const penumpang = this.jumlahPenumpang.value;

    console.log('tanggal', tanggal);
    console.log('jam', jam);
    console.log('penumpang', penumpang);

    const tanggalJam = new Date(`${tanggal} ${jam}`);
    console.log('tanggalJam', tanggalJam);

    const epochTime = tanggalJam.getTime();
    console.log('epochTime', epochTime);

    const cars = await Binar.listCars((item)=>{
      const filterByCapacity = item.capacity >= penumpang;
      const filterByTanggalSewa = item.availableAt.getTime() < epochTime;
      
      return filterByCapacity && filterByTanggalSewa;
    });
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}